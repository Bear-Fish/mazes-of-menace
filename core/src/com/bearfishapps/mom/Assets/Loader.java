package com.bearfishapps.mom.Assets;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Loader {
    private static Texture texture;
    public static TextureRegion touchpadKnob;
    public static TextureRegion touchpadBG;

    public static void load() {
        texture = new Texture("ui/ui.png");
        texture.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);

        touchpadKnob      = new TextureRegion(texture, 0, 64, 32, 32);
        touchpadBG      = new TextureRegion(texture, 0, 0, 64, 64);

    }

    public static void dispose() {
        texture.dispose();
    }
}
