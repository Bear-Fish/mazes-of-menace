package com.bearfishapps.mom.Characters;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.bearfishapps.mom.Tools.CustomMathClasses.CenteredRectangle;

import java.util.ArrayList;

public class Character extends CenteredRectangle{

    // for faceing direction and for attacking
    // TODO: player Attack
    private float rotation = 0;
    private float time = 0;
    private float fireInterval = 2;

    // for player scoring
    // TODO: player scoreing and death
    private int health = 100;
    private int totalHealth = health;

    private Color color;
    private LineOfSight sight;
    public Character(float x, float y, float width, float height, Color c, char[][] map) {
        super(x, y, width, height);
        color = c;
        sight = new LineOfSight(new Vector2(x, y), 50, map);
    }
    public void update(Vector2 nextPos) {
        this.setPosition(nextPos);
    }

    public void updateLineOfSight() {
        sight.update(getPosition());
    }

    public void draw(ShapeRenderer renderer) {
/*        renderer.setColor(Color.WHITE);
        renderer.circle(x, y, 50);

        for(Vector2 v: sight.seenPositions) {
            renderer.rect(v.x, v.y, 1, 1);
        }
*/
        color = new Color((totalHealth - health) / totalHealth, health / totalHealth, 0, 0.3f);
        renderer.setColor(color);
        renderer.rect(x, y, width, height);
    }

    public void setHealth(int hp) {
        health = hp;
        totalHealth = hp;
    }

    public void increaseHealth(int num) {
        health += num;
    }

    public void changeColor(Color c) {
        color = c;
    }

    public boolean isCollidedWith(Rectangle rect) {
        if(this.overlaps(rect))
            return true;
        return false;
    }

    public ArrayList<Vector2> getVisiblePositions() {
        return sight.seenPositions;
    }
}
