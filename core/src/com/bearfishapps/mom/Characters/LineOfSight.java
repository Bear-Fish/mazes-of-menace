package com.bearfishapps.mom.Characters;

import com.badlogic.gdx.math.Vector2;
import com.bearfishapps.mom.Constants.Constants;
import com.bearfishapps.mom.Tools.CustomMathClasses.Line;

import java.util.ArrayList;

public class LineOfSight {
    private float sightRadius = 20;
    private Vector2 center;
    private boolean[][] visiblePositions;
    private ArrayList<Line> walls = new ArrayList<Line>();
    public ArrayList<Vector2> seenPositions = new ArrayList<Vector2>();

    public LineOfSight(Vector2 pos, float sightRadius, char[][] map) {
        center = new Vector2(pos);
        connectInTwoDirections(map);
        sightRadius*=5;
        visiblePositions = new boolean[(int)sightRadius*2][(int)sightRadius*2];
        this.sightRadius = sightRadius;

        update(pos);
    }

    public void update(Vector2 pos) {
        //TODO: NEED TO REWORK THIS, it gives me 10 fps
        seenPositions.clear();
        center = new Vector2(pos);
        addAllItemsWithinRadius();
        removeBlindSpots();

        for(int i = 0; i < visiblePositions.length; i ++) {
            for(int j = 0; j < visiblePositions[0].length; j ++) {
                if (visiblePositions[i][j]) {
                    seenPositions.add(new Vector2(center.x+i-visiblePositions.length/2, center.y+j-visiblePositions[0].length/2));
                }
            }
        }
    }

    protected void removeBlindSpots() {
        for(int i = 0; i < visiblePositions.length; i ++) {
            for(int j = 0; j < visiblePositions[0].length; j ++) {
                if(visiblePositions[i][j]) {
                    Line line = new Line(new Vector2(center), new Vector2(center.x+i-visiblePositions.length/2, center.y+j-visiblePositions[0].length/2));
                    boolean intersection = false;
                    for(Line wall: walls) {
                        if(line.intersectsLine(wall))
                            intersection = true;
                    }

                    if(intersection)
                        visiblePositions[i][j] = false;
                }
            }
        }
    }

    protected void addAllItemsWithinRadius() {
        for(int i = -(int)sightRadius; i < (int)sightRadius; i ++) {
            for(int j = -(int)sightRadius; j < (int)sightRadius; j ++) {
                if(new Vector2(i, j).dst2(new Vector2(0, 0))< sightRadius) {
                    visiblePositions[i+visiblePositions.length/2][j+visiblePositions[0].length/2] = true;
                }else visiblePositions[i+visiblePositions.length/2][j+visiblePositions[0].length/2] = false;
            }
        }
    }

    protected void connectInTwoDirections(char[][] map) {
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                if (map[i][j] == Constants.mapConstants.wall) {
                    try {
                        if (map[i + 1][j] == Constants.mapConstants.wall)
                            walls.add(new Line(new Vector2(i, j), new Vector2(i + 1, j)));
                    } catch (Exception e) {
                    }
                    try {
                        if (map[i][j + 1] == Constants.mapConstants.wall)
                            walls.add(new Line(new Vector2(i, j), new Vector2(i, j + 1)));
                    } catch (Exception e) {
                    }
                }
            }
        }
    }

}
