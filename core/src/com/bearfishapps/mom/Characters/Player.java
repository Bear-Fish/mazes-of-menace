package com.bearfishapps.mom.Characters;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.bearfishapps.mom.Tools.Exceptions.CheatingVelocityException;

import java.util.ArrayList;

public class Player {
    private Character character;
    private Vector2 direction;
    private Vector2 prevPos;
    public Player(float x, float y, float size, Color c, char[][] map) {
        character = new Character(x+size/2, y+size/2, size, size, c, map);
        direction = new Vector2(0, 0);
        prevPos = new Vector2(x, y);
    }

    public float timeInterval = 100000;
    public void update(float delta) {
        timeInterval += delta;
        prevPos = new Vector2(character.getPosition());
        Vector2 temp = new Vector2(character.getPosition());
        temp.x += direction.x * 0.9f * delta;
        temp.y += direction.y * 0.9f * delta;
        character.update(temp);

        // Slows down the android app
        if (timeInterval > 0.25f) {
//            character.updateLineOfSight();
        }
    }

    public void draw(ShapeRenderer renderer) {
        character.draw(renderer);
    }

    public void setDirection(Vector2 d) throws CheatingVelocityException{
        if(d.x < -1 || d.x > 1)
            throw new CheatingVelocityException();
        if(d.y < -1 || d.y > 1)
            throw new CheatingVelocityException();

        direction = new Vector2(d);
    }

    public Vector2 getPosition() {
        return character.returnPosition();
    }

    public void retreatOnePos() {
        character.setPosition(prevPos);
    }

    public boolean isCollidedWithWalls(ArrayList<Rectangle> walls) {
        for(Rectangle r: walls) {
            if(character.overlaps(r))
                return true;
        }
        return false;
    }

    public ArrayList<Vector2> getVisiblePositions() {
        return character.getVisiblePositions();
    }
}
