package com.bearfishapps.mom.Constants;

public class Constants {

    private Constants() {

    }

    // Map symbols
    public static MapConstants mapConstants = new MapConstants();

    // item symbols
    public static ItemConstants itemConstants = new ItemConstants();
}
