package com.bearfishapps.mom.Constants;

public class MapConstants {
    public final char floor = '.';
    public final char wall = 'w';
    public final char unreachable = ' ';
    public final char start = 's';
    public final char end = 'e';
}
