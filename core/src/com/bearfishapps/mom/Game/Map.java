package com.bearfishapps.mom.Game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.bearfishapps.mom.Constants.Constants;

import java.util.ArrayList;
import java.util.HashSet;

public class Map {
    private char[][] map;
    private char[][] itemsMap;

    protected ArrayList<Rectangle> walls = new ArrayList<Rectangle>();
    //    protected ArrayList<Rectangle> floor = new ArrayList<Rectangle>();
//    protected ArrayList<Rectangle> outOFBounds = new ArrayList<Rectangle>();
    protected HashSet<Rectangle> rememberedWalls = new HashSet<Rectangle>();
    protected Rectangle start, end;

    protected ArrayList<Rectangle> items = new ArrayList<Rectangle>();

    public Map(char[][] generatedMap, char[][] itemMap) {
        map = generatedMap;
        this.itemsMap = itemMap;
        makeRectangles(Constants.mapConstants.wall, walls);
        makeRectangles(Constants.itemConstants.itemBox, items);
        start = makeOneRectangle(Constants.mapConstants.start);
        end = makeOneRectangle(Constants.mapConstants.end);
    }

    private ArrayList<Rectangle> visibleWalls = new ArrayList<Rectangle>();
    private ArrayList<Rectangle> visibleItems = new ArrayList<Rectangle>();
    private float totalTime = 100000;

    public void update(float delta, ArrayList<Vector2> visiblePositions) {
        // slows down the app to 10 fps on phones
        totalTime += delta;
        if (totalTime >= 0.25f) {
            for (Vector2 v : visiblePositions) {
                for (Rectangle w : walls) {
                    if (new Rectangle((int) v.x, (int) v.y, 1, 1).overlaps(w)) {
                        visibleWalls.add(new Rectangle((int) v.x, (int) v.y, 1, 1));
                        rememberedWalls.add(new Rectangle((int) v.x, (int) v.y, 1, 1));
                    }
                    if (new Rectangle((int) v.x + 1, (int) v.y + 1, 1, 1).overlaps(w)) {
                        visibleWalls.add(new Rectangle((int) v.x + 1, (int) v.y + 1, 1, 1));
                        rememberedWalls.add(new Rectangle((int) v.x + 1, (int) v.y + 1, 1, 1));
                    }
                }
                for (Rectangle i : items) {
                    if (new Rectangle((int) v.x, (int) v.y, 0.5f, 0.5f).overlaps(i))
                        visibleItems.add(new Rectangle(v.x, v.y, 0.5f, 0.5f));
                }
            }
        }
    }

    public void draw(ShapeRenderer renderer) {

        renderer.setColor(Color.FIREBRICK);
        for (Rectangle r : rememberedWalls) {
            renderer.rect(r.x, r.y, r.width, r.height);
        }

        for (Rectangle r : visibleWalls) {
            renderer.rect(r.x, r.y, r.width, r.height);
            break;
        }

        renderer.setColor(Color.BLUE);
        renderer.rect(start.x, start.y, start.width, start.height);
        renderer.setColor(Color.GREEN);
        renderer.rect(end.x, end.y, end.width, end.height);

        renderer.setColor(Color.ORANGE);
        for (Rectangle r : visibleItems) {
            renderer.rect(r.x, r.y, r.width, r.height);
        }

    }

    protected Rectangle makeOneRectangle(char type) {
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                if(map[i][j] == type)
                    return new Rectangle(i, j, 1, 1);
            }
        }
        return null;
    }

    protected void makeRectangles(char type, ArrayList<Rectangle> toBeAdded) {
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                if (map[i][j] == type) {
                    toBeAdded.add(new Rectangle(i, j, 1, 1));
                }
            }
        }
    }

    public ArrayList<Rectangle> getWalls() {
        return walls;
    }

    public Vector2 getStart() {
        return new Vector2(start.getX(), start.getY());
    }
}
