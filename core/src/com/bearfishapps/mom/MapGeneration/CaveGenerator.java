package com.bearfishapps.mom.MapGeneration;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.bearfishapps.mom.Constants.Constants;

public class CaveGenerator extends MapGenerator{
    public CaveGenerator(int sizeX, int sizeY) {
        super(sizeX, sizeY);
    }

    public void generate(float density) {
        // flood map with wall tiles
        flushMap();
        // change random tiles to floor tiles from wall tiles
        inverseRandomTiles();
        // execute the four five rule
        for(int i = 0; i < 4; i ++) {
            fourFiveRule();
        }
        // execute a modified four five rule
        modifiedFourFiveRule();

        // trace the borders fo the map
        traceBorders();
        spawnStartEndPositions();
        export("H:/map.txt");
        spawnItems();
        exportChars(itemsMap, "H:/map.txt");
    }

    protected void inverseRandomTiles() {
        for(int i = 0; i < map.length; i ++) {
            for(int j = 0; j < map[0].length; j ++) {
                if(MathUtils.random(1, 100) < 45) {
                    map[i][j] = Constants.mapConstants.floor;
                }
            }
        }
    }

    protected void fourFiveRule() {
        // The rule can be obtained from http://www.roguebasin.com/index.php?title=Cellular_Automata_Method_for_Generating_Random_Cave-Like_Levels
        // Basically, if a tiles is a floor tiles, if 4 of the 8 neighbour tiles are wall tiles, then it becomes a wall tile
        // If a tile is a wall tile, and if 5 of of the 8 neighbour tiles are floor tiles then it becomes a floor tile

        // used to figure out which tiles to invers
        boolean[][] inversed = new boolean[map.length][map[0].length];
        for(int i = 0 ; i < map.length; i ++) {
            for(int j = 0; j < map[i].length; j++) {
                inversed[i][j] = false;
                if(map[i][j] == Constants.mapConstants.wall) {
                    if (howSimilarAreItsNeighbours(i, j) <= 3 )
                        inversed[i][j] = true;
                }
                if(map[i][j] == Constants.mapConstants.floor) {
                    if (howSimilarAreItsNeighbours(i, j) <= 4)
                        inversed[i][j] = true;
                }
            }
        }

        // change the tiles
        inverseTiles(inversed);
    }

    protected void modifiedFourFiveRule() {
        // a copied verion of the fourFiveRule()
        boolean[][] inversed = new boolean[map.length][map[0].length];
        for(int i = 0 ; i < map.length; i ++) {
            for(int j = 0; j < map[i].length; j++) {
                inversed[i][j] = false;
                if(map[i][j] == Constants.mapConstants.wall) {
                    if (howSimilarAreItsNeighbours(i, j) <= 3 )
                        inversed[i][j] = true;
                }
                if(map[i][j] == Constants.mapConstants.floor) {
                    // the only difference is that if less than 2 of a floor tile's neighbours are wall tiles
                    // (meaning more than 6 are floor tile neighbours)
                    // then the tile also becomes a wall
                    if (howSimilarAreItsNeighbours(i, j) <= 4 || howSimilarAreItsNeighbours(i, j) >= 6 )
                        inversed[i][j] = true;
                }
            }
        }
        inverseTiles(inversed);
    }

    protected void inverseTiles(boolean[][] array) {
        for(int i = 0 ; i < map.length; i ++) {
            for (int j = 0; j < map[i].length; j++) {
                if(array[i][j]) {
                    if(map[i][j] == Constants.mapConstants.floor)
                        map[i][j] = Constants.mapConstants.wall;
                    if(map[i][j] == Constants.mapConstants.wall)
                        map[i][j] = Constants.mapConstants.floor;
                }
            }
        }
    }

    protected int howSimilarAreItsNeighbours(int x, int y) {
        // check the 8 surrounding neighbour tiles
        int count = 0;

        char compared = map[x][y];
        try {
            if (map[x - 1][y - 1] == compared) count++;
        }catch (Exception e) {count ++;}
        try {
        if(map[x-1][y] == compared) count ++;
        }catch (Exception e) {count ++;}
        try {
        if(map[x-1][y+1] == compared) count ++;
        }catch (Exception e) {count ++;}
        try {
        if(map[x][y-1] == compared) count ++;
        }catch (Exception e) {count ++;}
        try {
        if(map[x][y+1] == compared) count ++;
        }catch (Exception e) {count ++;}
        try {
        if(map[x+1][y-1] == compared) count ++;
        }catch (Exception e) {count ++;}
        try {
        if(map[x+1][y] == compared) count ++;
        }catch (Exception e) {count ++;}
        try {
        if(map[x+1][y+1] == compared) count ++;
        }catch (Exception e) {count ++;}

        return count;
    }
}
