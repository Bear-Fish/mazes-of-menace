package com.bearfishapps.mom.MapGeneration;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.bearfishapps.mom.Constants.Constants;

import java.util.ArrayList;

public class MapGenerator {
    protected ArrayList<Rectangle> rooms = new ArrayList<Rectangle>();
    protected ArrayList<Vector2> roomCenterPos = new ArrayList<Vector2>();
    protected char[][] map;
    protected char[][] itemsMap;
    protected int minRoomSize, maxRoomSize, mapX, mapY;

    public MapGenerator(int sizeX, int sizeY) {
        map = new char[sizeX][sizeY];
        itemsMap = new char[sizeX][sizeY];

        minRoomSize = 3;
        maxRoomSize = (int) Math.sqrt(((sizeX + sizeY)));
        Gdx.app.log("Map", "Dimentions " + sizeX + ", " + sizeY);
        Gdx.app.log("Rooms", "Size rangeing from " + minRoomSize + "to " + maxRoomSize);

        mapX = sizeX;
        mapY = sizeY;
    }

    public void generate(float density) {
        flushMap();
        generateRooms(density);
        Gdx.app.log("Rooms", "Generation Complete | Total of " + rooms.size() + " rooms");
        generateCorridors();
        Gdx.app.log("Corridors", "Generation Complete");
        cleanUp();
        traceBorders();
        spawnStartEndPositions();
//        export("H:/map.txt");

        spawnItems();
//        exportChars(itemsMap, "H;/map.txt");
    }

    public char[][] getMap() {
        return map;
    }

    public char[][] getItemsMap() {
        return itemsMap;
    }

    public void export(String location) {
        exportChars(map, location);
    }

    public static void exportChars(char[][] output, String location) {
        FileHandle fileHandle;
        Gdx.app.log("Map File", "Saveing ...");
        // absolute location of a file
        fileHandle = Gdx.files.absolute(location);
        // string for exports to be located
        String content = "";

        Gdx.app.log("Map File", "Preparing data ...");
        // note that you need to turn your head 90 degrees in order to view it properly
        for (int i = 0; i < output.length; i++) {
            for (int j = 0; j < output[i].length; j++) {
                content += output[i][j];
            }
            content += "\n";
        }

        Gdx.app.log("Map File", "Writing To File ...");
        fileHandle.writeString(content, false);

    }

    ///////////////side functions///////////////////

    // flush map with walls
    protected void flushMap() {
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                map[i][j] = Constants.mapConstants.wall;
            }
        }
    }

    // flush item map with blanks
    protected void flushItemMap() {
        for (int i = 0; i < itemsMap.length; i++) {
            for (int j = 0; j < itemsMap[i].length; j++) {
                itemsMap[i][j] = Constants.itemConstants.blank;
            }
        }
    }

    ////////////////Corridor Generating //////////////////////
    protected void generateCorridors() {
        int count = 0;
        for (Vector2 room : roomCenterPos) {
            Gdx.app.log("Corridor", "Room Centered Position: " + room);
            Vector2[] targetPoints = getTwoClosestRooms(count, roomCenterPos);
            makeCorridors(room, targetPoints[0]);
            makeCorridors(room, targetPoints[1]);

            count++;
        }
    }

    protected void makeCorridors(Vector2 startPoint, Vector2 endPoint) {
        Gdx.app.log("Corridors", "From: " + startPoint + ", To: " + endPoint);
        if (Math.random() < 0.5f) {
            int i;
            if ((endPoint.x - startPoint.x) > 0) {
                for (i = 0; i < endPoint.x - startPoint.x; i++) {
                    map[(int) startPoint.x + i][(int) startPoint.y] = Constants.mapConstants.floor;
                }
            } else {
                for (i = 0; i > endPoint.x - startPoint.x; i--) {
                    map[(int) startPoint.x + i][(int) startPoint.y] = Constants.mapConstants.floor;
                }
            }
            if ((endPoint.y - startPoint.y) > 0) {
                for (int j = 0; j < endPoint.y - startPoint.y; j++) {
                    map[(int) startPoint.x + i][(int) startPoint.y + j] = Constants.mapConstants.floor;
                }
            } else {
                for (int j = 0; j > endPoint.y - startPoint.y; j--) {
                    map[(int) startPoint.x + i][(int) startPoint.y + j] = Constants.mapConstants.floor;
                }
            }
        } else {
            int j;
            if ((endPoint.y - startPoint.y) > 0) {
                for (j = 0; j < endPoint.y - startPoint.y; j++) {
                    map[(int) startPoint.x][(int) startPoint.y + j] = Constants.mapConstants.floor;
                }
            } else {
                for (j = 0; j > endPoint.y - startPoint.y; j--) {
                    map[(int) startPoint.x][(int) startPoint.y + j] = Constants.mapConstants.floor;
                }
            }
            if ((endPoint.x - startPoint.x) > 0) {
                for (int i = 0; i < endPoint.x - startPoint.x; i++) {
                    map[(int) startPoint.x + i][(int) startPoint.y + j] = Constants.mapConstants.floor;
                }
            } else {
                for (int i = 0; i > endPoint.x - startPoint.x; i--) {
                    map[(int) startPoint.x + i][(int) startPoint.y + j] = Constants.mapConstants.floor;
                }
            }

        }
    }

    protected Vector2[] getTwoClosestRooms(int comparedRoom, ArrayList<Vector2> roomCenterPos) {
        Vector2[] rooms = {new Vector2(-100, -100), new Vector2(-100, -100)};
        Vector2 originalRooms = roomCenterPos.get(comparedRoom);
        float distance = 999999999;

        int count = 0;
        for (Vector2 roomPos : roomCenterPos) {
            if (originalRooms.dst2(roomPos) < distance) {
                if (count % 2 == 0) {
                    rooms[0] = new Vector2(roomPos);
                } else {
                    rooms[1] = new Vector2(roomPos);
                }
            }
            count++;
        }

        return rooms;
    }

    ///////////////room generating///////////////////////////
    protected void generateRooms(float density) {
        while (getDensity(getTileCount(Constants.mapConstants.floor), map.length * map[0].length) < density) {
            while (true) {
                char[][] room;
                Rectangle roomRect;
                while (true) {
                    boolean overlaps = false, outOfX = false, outOfY = false;
                    room = makeRooms(MathUtils.random(minRoomSize, maxRoomSize), MathUtils.random(minRoomSize, maxRoomSize));
                    roomRect = makeRoomRectengle(MathUtils.random(1, mapX - 1), MathUtils.random(1, mapY - 1), room);
                    Gdx.app.log("Rooms", "considering [" + roomRect + "]");
                    for (Rectangle otherRooms : rooms) {
                        if (otherRooms.overlaps(roomRect)) {
                            overlaps = true;
                            break;
                        }
                        if (roomRect.x + roomRect.width >= mapX - 1 || roomRect.x < 0) {
                            outOfX = true;
                            break;
                        }
                        if (roomRect.y + roomRect.height >= mapY - 1 || roomRect.y < 0) {
                            outOfY = true;
                            break;
                        }
                    }
                    if (overlaps || outOfX || outOfY) {
                        Gdx.app.log("Rooms", "BAD location");
                        continue;
                    }
                    rooms.add(roomRect);
                    break;
                }
                if (addToMap((int) roomRect.x + 1, (int) roomRect.y + 1, room))
                    break;
            }
        }
    }

    //////////////////Spawn Special Tiles //////////////////

    protected void spawnStartEndPositions() {
        int x;
        int y;
        while(true) {
            x = MathUtils.random(0, map.length-1);
            y = MathUtils.random(0, map[0].length-1);
            if(map[x][y] == Constants.mapConstants.floor) {
                break;
            }
        }

        float distance = 0;
        int x2 = 0, y2 = 0;
        int i, j;
        for(i = 0; i < map.length; i++) {
            for (j = 0; j < map[i].length; j++) {
                if(map[i][j] == Constants.mapConstants.floor)
                    if(new Vector2(i, j).dst2(x, y) > distance) {
                        distance = new Vector2(i, j).dst2(x, y);
                        x2 = i; y2 = j;
                    }
            }
        }

        map[x][y] = Constants.mapConstants.start;
        map[x2][y2] = Constants.mapConstants.end;

    }

    public void spawnItems() {
        flushItemMap();
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[0].length; j++) {
                if (map[i][j] == Constants.mapConstants.floor) {
                    if (Math.random() < 0.01) {
                        itemsMap[i][j] = Constants.itemConstants.itemBox;
                    }
                }
            }
        }
    }


    // this rectangle is for overlap detection
    protected Rectangle makeRoomRectengle(int x, int y, char[][] room) {
        Rectangle rect = new Rectangle(x - 1, y - 1, room.length + 1, room[0].length + 1);
        return rect;
    }

    // make room
    protected char[][] makeRooms(int width, int height) {
        char[][] roomvalues = new char[width][height];
        for (int i = 0; i < roomvalues.length; i++) {
            for (int j = 0; j < roomvalues[i].length; j++) {
                roomvalues[i][j] = Constants.mapConstants.floor;
            }
        }
        return roomvalues;
    }

    ///////////////////// map editing////////////
    // put room and etc to the map
    protected boolean addToMap(int initialX, int initialY, char[][] stuff) {
        try {
            Gdx.app.log("Rooms", "@ [" + initialX + ", " + initialY + ", " + stuff.length + ", " + stuff[0].length + "]");
            for (int i = 0; i < stuff.length; i++) {
                for (int j = 0; j < stuff[i].length; j++) {
                    map[initialX + i][initialY + j] =
                            stuff[i][j];
                }
            }
            roomCenterPos.add(new Vector2(initialX + stuff.length / 2, initialY + stuff[0].length / 2));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    //////////////// Trace Broders ///////////////
    protected void traceBorders() {
        Gdx.app.log("Map", "Tracing Borders ...");
        int i = 0;
        int j = 0;
        for (i = 0; i < map.length; i++) {
            map[i][0] = Constants.mapConstants.wall;
        }
        i --;
        for (j = 0; j < map[i].length; j++) {
            map[i][j] = Constants.mapConstants.wall;
        }
        i = 0;
        for (j = 0; j < map[i].length; j++) {
            map[i][j] = Constants.mapConstants.wall;
        }
        j--;
        for (i = 0; i < map.length; i++) {
            map[i][j] = Constants.mapConstants.wall;
        }
    }

    ////////////////Clean Up ///////////////////
    protected void cleanUp() {
        Gdx.app.log("Map", "Cleaning Up ... ");
        boolean[][] remove = new boolean[mapX][mapY];
        Gdx.app.log("Map", "Cleaning Up: Mapping Locations");
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                remove[i][j] = checkSurroundingSimilarities(map, new Vector2(i, j), 8, Constants.mapConstants.wall);
            }
        }
        Gdx.app.log("Map", "Cleaning Up: Writing");
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                if (remove[i][j])
                    map[i][j] = Constants.mapConstants.unreachable;
            }
        }
    }

    protected boolean checkSurroundingSimilarities(char[][] map, Vector2 location, int requiredCount, char character) {
        int count = 0;
        try {
            if (map[(int) location.x - 1][(int) location.y - 1] == character) count++;
            if (map[(int) location.x - 1][(int) location.y] == character) count++;
            if (map[(int) location.x - 1][(int) location.y + 1] == character) count++;
            if (map[(int) location.x][(int) location.y - 1] == character) count++;
            if (map[(int) location.x][(int) location.y + 1] == character) count++;
            if (map[(int) location.x + 1][(int) location.y - 1] == character) count++;
            if (map[(int) location.x + 1][(int) location.y] == character) count++;
            if (map[(int) location.x + 1][(int) location.y + 1] == character) count++;

            if (count >= requiredCount) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    ////////////////////misc////////////////////
    protected int getTileCount(char tileType) {
        int count = 0;
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                if (map[i][j] == tileType)
                    count++;
            }
        }
        return count;
    }

    // count the percentage of tiles turned over
    protected float getDensity(float tiles, float total) {
        float result = (tiles / total);
        Gdx.app.log("Map", "Tile: " + tiles + "  Total: " + total + " Density: " + result);
        return result;
    }
}
