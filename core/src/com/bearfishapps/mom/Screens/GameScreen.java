package com.bearfishapps.mom.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.bearfishapps.mom.Assets.Loader;
import com.bearfishapps.mom.Characters.Player;
import com.bearfishapps.mom.Game.Map;
import com.bearfishapps.mom.MapGeneration.CaveGenerator;
import com.bearfishapps.mom.MapGeneration.MapGenerator;
import com.bearfishapps.mom.MazesOfMenace;
import com.bearfishapps.mom.Tools.CustomUIClasses.CustomTouchPad;

public class GameScreen extends Screens {
    private Map gameMap;
    private Player player;
    private InputProcessor inputProcessor;

    private Touchpad touchpad;
    private Loader assetLoader;

    public GameScreen(MazesOfMenace game) {
        super(game);

        MapGenerator generator = new MapGenerator(50, 50);
        generator.generate(MathUtils.random(0.02f, 0.35f));
        Gdx.app.log("Map Generator", "Complete");

/*        CaveGenerator generator = new CaveGenerator(100, 100);
        generator.generate(10);
        Gdx.app.log("Cave Generator", "Complete");
*/
        gameMap = new Map(generator.getMap(), generator.getItemsMap());
        player = new Player(gameMap.getStart().x, gameMap.getStart().y, 0.2f, Color.RED, generator.getMap());

        inputProcessor = new InputProcessor() {
            @Override
            public boolean keyDown(int keycode) {
                Vector3 camCurrentPos = new Vector3(camera.position);
                switch (keycode) {
                    case Input.Keys.W:
                        camCurrentPos.y += 12f * camera.zoom;
                        camera.position.set(camCurrentPos);
                        camera.update();
                        break;
                    case Input.Keys.A:
                        camCurrentPos.x -= 12f * camera.zoom;
                        camera.position.set(camCurrentPos);
                        camera.update();
                        break;
                    case Input.Keys.S:
                        camCurrentPos.y -= 12f * camera.zoom;
                        camera.position.set(camCurrentPos);
                        camera.update();
                        break;
                    case Input.Keys.D:
                        camCurrentPos.x += 12f * camera.zoom;
                        camera.position.set(camCurrentPos);
                        camera.update();
                        break;
                    case Input.Keys.UP:
                        player.setDirection(new Vector2(0, 1));
                        break;
                    case Input.Keys.LEFT:
                        player.setDirection(new Vector2(-1, 0));
                        break;
                    case Input.Keys.DOWN:
                        player.setDirection(new Vector2(0, -1));
                        break;
                    case Input.Keys.RIGHT:
                        player.setDirection(new Vector2(1, 0));
                        break;
                    case Input.Keys.EQUALS:
                        if (camera.zoom > 0.01) camera.zoom /= 2;
                        camera.update();
                        break;
                    case Input.Keys.MINUS:
                        camera.zoom *= 2;
                        camera.update();
                        break;

                    default:
                        break;
                }
                return false;
            }

            @Override
            public boolean keyUp(int keycode) {
                return false;
            }

            @Override
            public boolean keyTyped(char character) {
                return false;
            }

            @Override
            public boolean touchDown(int screenX, int screenY, int pointer, int button) {
                return false;
            }

            @Override
            public boolean touchUp(int screenX, int screenY, int pointer, int button) {
                return false;
            }

            @Override
            public boolean touchDragged(int screenX, int screenY, int pointer) {
                return false;
            }

            @Override
            public boolean mouseMoved(int screenX, int screenY) {
                return false;
            }

            @Override
            public boolean scrolled(int amount) {
                return false;
            }
        };

        //////////////////// assets stuff afer this line
        assetLoader = new Loader();
        assetLoader.load();

        CustomTouchPad.make(assetLoader.touchpadBG, assetLoader.touchpadKnob);
        touchpad = new Touchpad(3f, CustomTouchPad.style);
    }

    @Override
    public void draw(float delta, float animationKeyFrame) {
//        gameMap.update(delta, player.getVisiblePositions());
        player.setDirection(new Vector2(touchpad.getKnobPercentX(), touchpad.getKnobPercentY()));

        player.update(delta);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        gameMap.draw(shapeRenderer);
        player.draw(shapeRenderer);
        shapeRenderer.end();
        if(player.isCollidedWithWalls(gameMap.getWalls())) {
            player.retreatOnePos();
        }

        Gdx.app.log("FPS", String.valueOf(1 / delta));
    }

    @Override
    public void preShow(Table table, InputMultiplexer multiplexer) {
        table.bottom().left();
        table.add(touchpad).width(200).height(200);
        multiplexer.addProcessor(inputProcessor);
    }

    @Override
    public void dispose() {
        super.dispose();
        assetLoader.dispose();
    }
}
