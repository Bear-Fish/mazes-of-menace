package com.bearfishapps.mom.Tools.CustomMathClasses;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

// This class is created to edit the libgdx Rectangle, so that we can have automatically centered objects

public class CenteredRectangle extends Rectangle {
    public CenteredRectangle(float x, float y, float width, float height) {
        super(x - width / 2, y - height / 2, width, height);
    }

    public Vector2 returnPosition() {
        return new Vector2(x + height / 2, y + width / 2);
    }

    public Vector2 getPosition() {
        return new Vector2(x, y);
    }

}
