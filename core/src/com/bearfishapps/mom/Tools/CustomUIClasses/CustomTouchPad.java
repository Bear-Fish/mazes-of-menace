package com.bearfishapps.mom.Tools.CustomUIClasses;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class CustomTouchPad{
    public static Touchpad.TouchpadStyle style;

    public static void make(TextureRegion background, TextureRegion knob) {

        style = new Touchpad.TouchpadStyle();
        style.background = new TextureRegionDrawable(background);
        style.knob = new TextureRegionDrawable(knob);
    }
}
