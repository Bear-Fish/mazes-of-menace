package com.bearfishapps.mom.Tools.Exceptions;

public class CheatingVelocityException extends RuntimeException{
    public CheatingVelocityException() {
        super("You have cheated the system with a super-fast velocity!!! Please use numbers between 1 and -1");
    }
}
