package com.bearfishapps.mom.Tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

// very self explanitory, used to store preferences

public class Prefs {
    private static Preferences prefs;

    public static void init() {
        prefs = Gdx.app.getPreferences("com.bearfishapps.ai2m");
        if (!prefs.contains("score")) {
            prefs.putInteger("score", -10000000);
            prefs.flush();
        }
    }

    public static void setScore(int score) {
        prefs.putInteger("score", score);
        prefs.flush();
    }

    public static int getScore() {
        return prefs.getInteger("score");
    }
}
