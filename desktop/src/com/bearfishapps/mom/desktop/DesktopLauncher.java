package com.bearfishapps.mom.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.bearfishapps.mom.MazesOfMenace;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
//		config.resizable = false;
		config.width = 800;
		config.height = 480;
		new LwjglApplication(new MazesOfMenace(), config);
	}
}
